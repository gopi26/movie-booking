# Movie Booking
**overview**\
We need to design an online movie ticket booking system where a user can search for a movie in a given city and book it.\
In order to do this, we have decomposed the entire movie ticket booking application into various epics. In each epic you will be working on various user stories within a given sprint.\
Currently, you are in sprint – 1 which is two weeks' timeline, and as part of your user story 1, you are going to design a login widget. In this sprint, you will be working as a front-end design. Your task details are given below.

|     Epic          |     User Management                   |     Epic ID          |     E-001       |
|-------------------|---------------------------------------|----------------------|-----------------|

|     Sprint        |     Sprint 1                          |
|-------------------|---------------------------------------|

|     User Story    |     Login Widget                     |     User story ID    |     US-001      |
|-------------------|---------------------------------------|----------------------|-----------------|

|     Task          |     Login page                |     Task ID          |     TE-T-001    |
|-------------------|---------------------------------------|----------------------|-----------------|

|     Role          |     Design - Frontend                 |
|-------------------|---------------------------------------|

|     Tech Stack    |     React    |   
|-------------------|--------------|

